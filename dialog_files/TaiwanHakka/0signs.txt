﻿Transcription signs

L2	Taiwan Mandarin (marked as @s)
L3	Taiwan Southern Min (marked as @s:min)
L4	Japanese  (marked as @s:jpn)
L5	English  (marked as @s:eng)

in conversation:
\ud840 -> ʘ  0298
\ud84a -> ǁ  01C1

in narrative:
\ud840 -> 