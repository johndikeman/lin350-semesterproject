@UTF8
CMDI_PID:	11312/a-00005118-1
Title:	Taiwan Mandarin Corpus
Creator:	Chui, Kawai
Creator:	Lai, Huei-ling
Subject:	conversation analysis
Subject.olac:linguistic-field:	
Subject.olac:language:	zho, min
Description:	 conversations
Publisher:	TalkBank
Contributor:	
Date:		2017
Type:	Text
Type:	Sound
Type.olac:linguistic-type:	primary_text
Type.olac:discourse-type:	dialogue
Format:		 
Language:	
Relation:	
Coverage:	
Rights:	
IMDI_Genre:	discourse
IMDI_Interactivity:	interactive
IMDI_PlanningType:	spontaneous
IMDI_Involvement:	non-elicited
IMDI_SocialContext:	family
IMDI_EventStructure:	conversation
IMDI_Task:	unspecified
IMDI_Modalities:	Spoken
IMDI_Subject:	unspecified
IMDI_EthnicGroup:	unspecified
IMDI_RecordingConditions:	unspecified
IMDI_AccessAvailability:	open access
IMDI_Continent:	Asia
IMDI_Country:	Taiwan
IMDI_ProjectDescription:	
IMDI_MediaFileDescription:	
IMDI_WrittenResourceSubType:	

DOI:	doi:10.21415/T5DT2T
